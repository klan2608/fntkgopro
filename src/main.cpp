#include "stm32f10x.h"
#include <gpio.h>
#include <delay.h>
#include <gopro.h>
#include <protocol.h>
#include <usart.h>
#include <config.h>

using namespace GpioName;
using namespace DelayName;
using namespace GoProName;
using namespace ProtocolName;
using namespace UsartName;
using namespace ConfigName;

Gpio* led;
GoPro* gopro;
Protocol* protocolMUSV;
Usart* uartUAV;

void UpdateConfig(ConfigurationMUSV_TypeDef* configInput);

int main()
{
	Delay::Init();

	gopro = new GoPro(GpioPort_A, 12,
			GpioPort_A, 11,
			GpioPort_B, 3);

	gopro->Power(true);
	led = new Gpio(GpioPort_D, 1, GpioMode_Inverted_OD);
	protocolMUSV = new Protocol(500);
	uartUAV = new Usart(Usart_1, 115200);

	while (1)
    {
		protocolMUSV->DecodingData(uartUAV->GetReadData(), uartUAV->GetReadDataSize());
		UpdateConfig(protocolMUSV->GetConfig());
		gopro->Update();
    }
}

void UpdateConfig(ConfigurationMUSV_TypeDef* configInput)
{
	if(configInput->newData.cameraModeChange)
	{
		switch(configInput->settings.cameraMode)
		{
		case SettingCameraMode_Photo:
			gopro->ChangeMode(GoProMode_Photo);
			break;
		case SettingCameraMode_TV:
			gopro->ChangeMode(GoProMode_Video);
			break;
		default:
			break;
		}
		configInput->newData.cameraModeChange = false;
	}
	if(configInput->newData.photoModeChange)
	{
		switch(configInput->settings.photoMode.mode)
		{
		case PhotoMode_Single:
			gopro->TakeFoto();
			configInput->settings.photoMode.mode = PhotoMode_NoShot;
			break;
		case PhotoMode_Burst:
			gopro->BurstMode(true, (uint32_t) (configInput->settings.photoMode.delay*10000));
			break;
		case PhotoMode_Video:
			gopro->Record(true);
			break;
		case PhotoMode_NoShot:
			gopro->BurstMode(false, 0);
			gopro->Record(false);
			break;
		default:
			break;
		}
		configInput->newData.photoModeChange = false;
	}
	if(configInput->settings.formatFlash)
	{
		gopro->FormatMemory();
		configInput->settings.formatFlash = false;
	}
}

extern "C"
{
void DMA1_Channel4_IRQHandler(void)
{
	if (DMA_GetITStatus(DMA1_IT_TC4))
	{
		uartUAV->dataSendingComplete = true;
		DMA_Cmd(DMA1_Channel4, DISABLE);
		DMA_ClearITPendingBit(DMA1_IT_TC4);
		uartUAV->StartTransmite();
	}
}
}
