/*
 * usart.h
 *
 *  Created on: 19 апр. 2015 г.
 *      Author: klan
 */

#ifndef INCLUDE_USART_H_
#define INCLUDE_USART_H_

#include "misc.h"

#include "stm32f10x_usart.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_dma.h"
#include "string.h"

namespace UsartName {

struct Data
{
	uint8_t* buffer;
	uint16_t size;
};

typedef enum
{
	UsartState_Off = 0,
	UsartState_On,
	UsartState_Init,
	UsartState_Deinit,
	UsartState_Error
} UsartState_TypeDef;

typedef enum
{
	Usart_1,
	Usart_1_Remap,
	Usart_2
} Usart_TypeDef;

class Usart {
	UsartState_TypeDef state;
	USART_TypeDef* usart;
	GPIO_TypeDef* port;
	DMA_Channel_TypeDef* dmaChannelTx;
	DMA_Channel_TypeDef* dmaChannelRx;
	Usart_TypeDef usartName;
	uint8_t BufferForSendQueue[500];
	uint32_t DataForSendSizeQueue;
	uint8_t BufferForSend[500];
	uint8_t BufferForRead[128];


public:
	bool dataSendingComplete;
	bool BufferForSendQueueHoldOn;
	Usart(Usart_TypeDef usartName, uint32_t baudRate);
	~Usart();
	void Switch(UsartState_TypeDef state);
	void SendData(uint8_t* data, uint16_t size);
	void StartTransmite();
	uint8_t* GetReadData(void);
	uint16_t GetReadDataSize(void);
	uint8_t* GetSendData(void);
	uint16_t GetSendDataSize(void);
	inline uint16_t GetReadBufferSize(){return (uint16_t) (sizeof(BufferForRead));}
};

}  // namespace UsartName



#endif /* INCLUDE_USART_H_ */
