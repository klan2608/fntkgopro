/**
 * @file delay.h
 * @author Y.Reznikov
 * @version 1.0.0
 * @date Апрель 2015 года
*/

#ifndef INCLUDE_DELAY_H_
#define INCLUDE_DELAY_H_
#include "stm32f10x_rcc.h"

/// @brief Управление временем микроконтроллера и задержками
namespace DelayName
{
///@brief Статический класс для работы с временем микроконтроллера и задержками
class Delay
{
	static volatile uint64_t workTimeSys;  	///< счетчик текущего времени
	static volatile uint64_t delayTime;		///< время задержки

public:
	static void Init();
	static void Tick();
	static void Wait(uint32_t delay);
	static uint64_t GetMicrosecond();
	static uint32_t GetMillesecond();
};

}

#endif /* INCLUDE_DELAY_H_ */
