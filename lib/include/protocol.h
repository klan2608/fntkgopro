/*
 * protocol.h
 *
 *  Created on: 20 апр. 2015 г.
 *      Author: klan
 */

#ifndef INCLUDE_PROTOCOL_H_
#define INCLUDE_PROTOCOL_H_

#include "config.h"
#include "string.h"

#define MSG_TYPE_UAV 				0x02
#define MSG_TYPE_STATE_REQ 			0x04
#define MSG_TYPE_ANGLES 			0x06
#define MSG_TYPE_SETTINGS 			0x08
#define MSG_TYPE_ANGULAR_SPEED 		0x0A
#define MSG_TYPE_PHOTO_VIDEO 		0x0C
#define MSG_TYPE_SENSOR_ZERO 		0x0E
#define MSG_TYPE_SERVICE			0x54

#define MSG_TYPE_OUT_ANGLES 		0x03
#define MSG_TYPE_OUT_STATE 			0x01
#define MSG_TYPE_OUT_SERVICE		0x55


#define MSG_DATA_POWER				0x01
#define MSG_DATA_PHOTO				0x02
#define MSG_DATA_ZOOM				0x03
#define MSG_DATA_EXPOSURE			0x04
#define MSG_DATA_FREQUENCY			0x05
#define MSG_DATA_STABILISATION		0x06
#define MSG_DATA_CONTROL_PWM		0x07
#define MSG_DATA_COLOR				0x08
#define MSG_DATA_CAMERA				0x09
#define MSG_DATA_FOCUS				0x0A
#define MSG_DATA_CALIBRATION		0x0B
#define MSG_DATA_MOTOR				0x0C
#define MSG_DATA_FORMAT_FLASH		0x0D

#define MSG_DATA_SERVICE_REQ_ROLL	0x00
#define MSG_REG_PID_ROLL_Kp			0x01
#define MSG_REG_PID_ROLL_ScopeKp	0x02
#define MSG_REG_PID_ROLL_Ki			0x03
#define MSG_REG_PID_ROLL_ScopeKi	0x04
#define MSG_REG_PID_ROLL_Kd			0x05
#define MSG_REG_PID_ROLL_ScopeKd	0x06
#define MSG_REG_PID_ROLL_L			0x07
//******************
#define MSG_DATA_SERVICE_REQ_PITCH	0x08
#define MSG_REG_PID_PITCH_Kp		0x09
#define MSG_REG_PID_PITCH_ScopeKp	0x0A
#define MSG_REG_PID_PITCH_Ki		0x0B
#define MSG_REG_PID_PITCH_ScopeKi	0x0C
#define MSG_REG_PID_PITCH_Kd		0x0D
#define MSG_REG_PID_PITCH_ScopeKd	0x0E
#define MSG_REG_PID_PITCH_L			0x0F
//******************
#define MSG_DATA_SERVICE_REQ_YAW	0x10
#define MSG_REG_PID_YAW_Kp			0x11
#define MSG_REG_PID_YAW_ScopeKp		0x12
#define MSG_REG_PID_YAW_Ki			0x13
#define MSG_REG_PID_YAW_ScopeKi		0x14
#define MSG_REG_PID_YAW_Kd			0x15
#define MSG_REG_PID_YAW_ScopeKd		0x16
#define MSG_REG_PID_YAW_L			0x17

#define MSG_DATA_SERVICE_REQ_CALIB	0x18
#define MSG_DATA_SERVISE_CALIB_DATA	0x19





namespace ProtocolName {

/**
 *
 */
class Protocol {
	ConfigName::ConfigurationMUSV_TypeDef config;
	uint8_t buffer[200];
	uint16_t sizeInputBuffer;
	uint16_t sizeOutputBuffer;
	uint16_t head;
	uint16_t tail;
	uint8_t eepromID;

	int FindNextHead(uint8_t* data, int startIndex, uint16_t head, uint16_t tail);
	bool CheckSum(uint8_t* data, uint16_t startIndex, uint16_t size);
	float ByteToFloat(uint8_t* data, uint16_t startIndex);
	void FloatToByte(float number, uint8_t* data, uint16_t startIndex);
	uint16_t CheckIter(uint16_t iter);
public:
	Protocol(uint16_t sizeBuffer);
	~Protocol();
	void SetConfig(ConfigName::ConfigurationMUSV_TypeDef* config);
	ConfigName::ConfigurationMUSV_TypeDef* GetConfig();
	void DecodingData(uint8_t* data, uint16_t size);
	void CodingOutputPID(uint8_t ID, float value);
	void CodingOutputCalibData(float value1,float value2,float value3,float value4,float value5,float value6);
	void CodingOutputAngleData(float value1, float value2, float value3);
	void CodingOutputStateData(ConfigName::ConfigurationMUSV_TypeDef* config);
	inline uint8_t GetEepromID() {return eepromID;}
	inline uint8_t* GetData() {return buffer;}
	inline uint16_t GetDataSize() {return sizeOutputBuffer;}
	inline uint16_t GetHead() {return head;}
	inline uint16_t GetTail() {return tail;}
	inline void ResetDataSize() {sizeOutputBuffer = 0;}
};

}  // namespace ProtocolName



#endif /* INCLUDE_PROTOCOL_H_ */
