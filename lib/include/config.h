/**
 * @file config.h
 * @author Y.Reznikov
 * @version 1.0.0
 * @date Апрель 2015 года
*/


#ifndef INCLUDE_CONFIG_H_
#define INCLUDE_CONFIG_H_
#include "stm32f10x.h"

///@brief Конфигурация ЦН, режимы работы, состояние
namespace ConfigName {

// MsgData for MsgType = 6 (00000110b)
///@brief Углы наведения ЦН
struct InputAngles_TypeDef
{
	float roll;		///< Заданное угловое положение по крену
	float pitch;		///< Заданное угловое положение по тангажу
	float yaw;		///< Заданное угловое положение по рысканью
};

// MsgData for MsgType = 8 (00001000b)
///@brief Режимы питания
enum SettingPower_TypeDef : uint8_t
{
	SettingPower_On = 1,	///< ЦН включена
	SettingPower_Off = 2,	///< ЦН выключена
	SettingPower_Wait = 3	///< ЦН в режиме ожидания
};

///@brief Режимы фотоаппарата
enum PhotoMode_TypeDef : uint8_t
{
	PhotoMode_NoShot = 0,		///< Прекращение выполнения фотоснимков
	PhotoMode_Single = 3,		///< Выполнение одиночного фотоснимка
	PhotoMode_Burst = 5,		///< Выполнение серии фотоснимков
	PhotoMode_Video = 7		///< Включение записи видео
};

///@brief Режимы фотосъемки
struct SettingPhotoMode_TypeDef
{
	PhotoMode_TypeDef mode;	///< Режим фотоаппарата
	uint16_t delay;			///< Задержка между снимками
};

///@brief Режимы активной ЦН
enum SettingCameraMode_TypeDef : uint8_t
{
	SettingCameraMode_TV = 1,	///< Режим видеокамеры
	SettingCameraMode_IR = 2,	///< Режим инфракрасной камеры
	SettingCameraMode_Photo = 4	///< Режим фотоаппарата
};

///@brief Режимы выходного видеосигнала ЦН
enum SettingVideoMode_TypeDef : uint8_t
{
	SettingVideoMode_VBS = 1,	///< Аналоговый SD видеосигнал PAL
	SettingVideoMode_YPbPr = 2,	///< Аналоговый HD видеосигнал 720i, 1080i
	SettingVideoMode_BT656 = 4	///< Цифровой видеосигнал
};

///@brief Режимы фокусировки ЦН
enum SettingFocus_TypeDef : uint8_t
{
	SettingFocus_Off = 0,		///< Фокусировка выключена
	SettingFocus_Auto = 1,		///< Автоматическая фокусировка
	SettingFocus_Infinity = 2	///< Фокусировка на бесконечность
};

///@brief Настройки ЦН
struct Setting_TypeDef
{
	SettingPower_TypeDef power;			///< Режим питания
	SettingPhotoMode_TypeDef photoMode;		///< Режим фотосъемки
	uint8_t zoom;					///< Кратность увеличения от 1 до 255
	uint16_t exposure;				///< Время выдержки в миллисекундах
	uint8_t frequencyPosition;			///< Частота выдачи угловых положений ЦН
	bool stabilization;				///< Режим стабилизации
	bool formatFlash;				///< Форматирование флеш карты
	bool controlPWM;				///< Режим управления по ШИМ сигналам
	uint8_t colorMode;				///< Режим псевдоцветов
	bool calibration;				///< Режим калибровки магнитометра
	bool motorState;				///< Режим двигателей
	SettingCameraMode_TypeDef cameraMode;		///< Режим активной ЦН
	SettingVideoMode_TypeDef videoMode;		///< Режим выходного видеосигнала
	SettingFocus_TypeDef focus;			///< Фокусировка
};

///@brief Флаги изменения состояния настроек
struct SettingFlag_TypeDef
{
	bool powerChange;				///< Изменение режима питания
	bool photoModeChange;				///< Изменение режима фотосъемки
	bool zoomChange;				///< Изменение кратности увеличения
	bool exposureChange;				///< Изменение времени выдержки
	bool frequencyPositionChange;			///< Изменение частоты выдачи угловых положений
	bool stabilizationChange;			///< Изменение режима стабилизации
	bool controlPWMChange;				///< Изменение режима управления по ШИМ сигналам
	bool colorModeChange;				///< Изменение режима псевдоцветов
	bool cameraModeChange;				///< Изменение режима активной ЦН
	bool videoModeChange;				///< Изменение режима выходного видеосигнала
	bool focusChange;				///< Изменение фокусировки
	bool calibrationChange;				///< Изменение режима калибровки магнитометра

	bool requestStateChange;			///< Изменение запроса состояния
	bool requestCalibData;				///< Изменение запроса данных калибровки магнитометра

	bool sensorZero;				///< Изменение начального положения датчиков

	bool pidChange;					///< Изменение ПИД регулятора

	bool motorStateChange;				///< Изменение режима двигателей
};

// MsgData for MsgType = 3 (00000011b)
///@brief Угловые положения ЦН для передачи
struct OutputAngles_TypeDef
{
	float roll;			///< Угловое положение по крену
	float pitch;		///< Угловое положение по тангажу
	float yaw;			///< Угловое положение по рысканью
};

// MsgData for MsgType = 10 (00001010b)
///@brief Угловая скорость наведения ЦН
struct InputAngularVelocity_TypeDef
{
	float roll;		///< Угловая скорость наведения по крену
	float pitch;		///< Угловая скорость наведения по тангажу
	float yaw;		///< Угловая скорость наведения по рысканью
};

///@brief Коэффициенты ПИД регулятора
struct ConfigAxisPID_TypeDef
{
	float Kp;		///< Пропорциональный коэффициент
	float scopeKp;		///< Ограничение пропорциональной составляющей
	float Ki;		///< Интегральный коэффициент
	float scopeKi;		///< Ограничение интегральной составляюзей
	float Kd;		///< Дифференциальный коэффициент
	float scopeKd;		///< Ограничение дифференциальной составляющей
	float L;		///< Ограничение результирующего сигнала
	bool request;		///< Флаг изменения коэффициентов
};

///@brief Коэффициенты ПИД регульторов ЦН
struct ConfigPID_TypeDef
{
	ConfigAxisPID_TypeDef roll;	///< ПИД по крену
	ConfigAxisPID_TypeDef pitch;	///< ПИД по тангажу
	ConfigAxisPID_TypeDef yaw;	///< ПИД по рысканью
};

///@brief Конфигурация ЦН
typedef struct
{
	SettingFlag_TypeDef newData;					///< Флаги изменения состояния настроек
	bool requestState;						///< Флаг запроса состояния ЦН
	InputAngles_TypeDef inputAngles;				///< Углы наведения ЦН
	Setting_TypeDef settings;					///< Настройки ЦН
	OutputAngles_TypeDef outputAngles;				///< Угловые положения ЦН для передачи
	InputAngularVelocity_TypeDef inputAngularVelocity;		///< Угловая скорость наведения ЦН
	ConfigPID_TypeDef configPID;					///< Коэффициенты ПИД регульторов ЦН
} ConfigurationMUSV_TypeDef;


}  // namespace ConfigName



#endif /* INCLUDE_CONFIG_H_ */
