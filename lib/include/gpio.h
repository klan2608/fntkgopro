/**
 * @file gpio.h
 * @author Y.Reznikov
 * @version 1.0.0
 * @date Апрель 2015 года
*/

#ifndef INCLUDE_GPIO_H_
#define INCLUDE_GPIO_H_

#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
///@brief Настройка и управление режимами работы вывода
namespace GpioName {
///@brief Номер порта выводов
typedef enum : uint8_t
{
	GpioPort_A = 0,	///< Порт А
	GpioPort_B = 1,	///< Порт B
	GpioPort_C = 2,	///< Порт C
	GpioPort_D = 3,	///< Порт D
	GpioPort_E = 4,	///< Порт E
	GpioPort_F = 5,	///< Порт F
	GpioPort_G = 6,	///< Порт G
	GpioPort_H = 7,	///< Порт H
	GpioPort_I = 8	///< Порт I
} GpioPort_TypeDef;

///@brief Состояние вывода
typedef enum : uint8_t
{
	GpioState_Off = 0,	///< Вывод выключен
	GpioState_On = 1,	///< Вывод включен
	GpioState_Init = 2,	///< Вывод в режиме настройки
	GpioState_Deinit = 3,	///< Вывод сброшен в начальное состояние
	GpioState_Error = 4	///< Ошибка при работе с выводом
} GpioState_TypeDef;

///@brief Режим работы вывода
typedef enum : uint8_t
{
	GpioMode_Normal = 0,		///< Состояние вкл - высокий уровень, состояние выкл - низкий уровень
	GpioMode_Inverted = 1,		///< Состояние вкл - низкий уровень, состояние выкл - высокий уровень
	GpioMode_Normal_OD = 2,		///< Состояние вкл - неопределенное состояние, состояние выкл - низкий уровень
	GpioMode_Inverted_OD = 3,	///< Состояние вкл - низкий уровень, состояние выкл - неопределенное состояние
	GpioMode_Int_Rising = 4,	///< Режим работы с прерыванием по фронту
	GpioMode_Int_Falling = 5,	///< Режим работы с прерыванием по спаду
	GpioMode_Int_Rising_OD = 6,	///< Режим работы с прерыванием по фронту
	GpioMode_Int_Falling_OD = 7	///< Режим работы с прерыванием по спаду
} GpioMode_TypeDef;

///@brief Логический уровень вывода
typedef enum : uint8_t
{
	GpioLevel_Low = 0,	///< Низкий уровень
	GpioLevel_High = 1	///< Высокий уровень
} GpioLevel_TypeDef;

///@brief Настройка и управление режимами работы вывода
class Gpio
{
	GpioState_TypeDef state;	///< Состояние вывода
	GPIO_TypeDef* port;		///< Указатель на порт выводов
	uint16_t pin;			///< Номер вывода в порту
	uint16_t exti;			///< Номер прерывания
	GpioLevel_TypeDef offLevel;	///< Логический уровень для выключенного состояния вывода

	void Init(GpioPort_TypeDef port, uint8_t pin, GpioMode_TypeDef mode);
	void Deinit();

public:
	Gpio(GpioPort_TypeDef port, uint8_t pin, GpioMode_TypeDef mode);
	~Gpio();
	void Switch(GpioState_TypeDef state);
	GpioState_TypeDef GetState();
	void SetLevel(GpioLevel_TypeDef level);
	void Toggle();
};

}  // namespace GPIOName
#endif /* INCLUDE_GPIO_H_ */
