/**
 * @file gopro.h
 * @author Y.Reznikov
 * @version 1.0.0
 * @date Июль 2015 года
*/

#ifndef INCLUDE_GOPRO_H_
#define INCLUDE_GOPRO_H_

#include "config.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "gpio.h"
#include "delay.h"

///@brief Управление режимами работы камеры GoPro Hero 4 Black
namespace GoProName {

///@brief Состояние кнопки
enum GoProButton_TypeDef : uint8_t
{
	GoProButton_Off = 0,	///< Кнопка отпущенна
	GoProButton_On = 1		///< Кнопка нажата
};

///@brief Режим камеры
enum GoProMode_TypeDef : uint8_t
{
	GoProMode_Video = 0,				///< Режим видеосъемки
	GoProMode_Photo = 1,				///< Режим фотосъемки
	GoProMode_MultiPhoto = 2,			///< Режим скоростной фотосъемки
	GoProMode_PlayBack = 3,				///< Режим воспроизведения видео
	GoProMode_GlobalSettings = 4		///< Режим общих настроек камеры
};

///@brief Режим настройки камеры
enum GoProGlobalSettings_TypeDef : uint8_t
{
	GoProGlobalSettings_WiFi = 0,				///< Настройка WiFi соединения (default: OFF)
	GoProGlobalSettings_Rotate = 1,				///< Переворот изображения на 180 градусов (default: UP)
	GoProGlobalSettings_DefaultMode = 2,			///< Установка начального режима (default: VIDEO)
	GoProGlobalSettings_FastStart = 3,				///< Режим быстрого включения и начала записи (default: OFF)
	GoProGlobalSettings_Led = 4,		///< Режим индекации светодиода (default: 4)
	GoProGlobalSettings_Sound = 5,	///< Управление звуковыми оповещениями (default: 100%)
	GoProGlobalSettings_VideoOut = 6,	///< Настройка формата выходного видео (default: NTSC)
	GoProGlobalSettings_OSD = 7,		///< Наложение информации на экран (default: ON)
	GoProGlobalSettings_Sleep = 8,		///< Время перехода в спящий режим (dafault: NEVER)
	GoProGlobalSettings_Calendar = 9,		///< Настройка времени и даты
	GoProGlobalSettings_Delete = 10,		///< Форматирование карты памяти
	GoProGlobalSettings_Reset = 11,		///< Сброс настроек
	GoProGlobalSettings_Exit = 12		///< Выход (включаентся начальный режим)
};


///@brief Управление режимами работы камеры GoPro Hero 4 Black
class GoPro
{
	bool powerState;					///< Текушее состояние питания камеры
	uint64_t powerTime;					///< Время нажатия кнопки включения/выключения питания камеры
	bool powerFlag;						///< Флаг нажатия кнопки включения/выключения питания камеры

	GoProMode_TypeDef modeCurrent;			///< Текущий режим камеры
	uint64_t modeTime;					///< Время нажатия кнопки изменения режима
	bool modeFlag;						///< Флаг нажатия кнопки изменения режима
	bool modeFlagDelay;					///< Флаг отпускания кнопки изменения режима
	uint32_t modeDelayOn;					///< Время удержания кнопки изменения режима нажатой
	uint32_t modeDelayOff;					///< Время удержания кнопки изменения режима отпущенной

	uint64_t selectTime;				///< Время нажатия кнопки выбора
	bool selectFlag;					///< Флаг нажатия кнопки выбора
	bool selectFlagDelay;					///< Флаг отпускания кнопки выбора
	uint32_t selectDelayOn;					///< Время удержания кнопки выбора нажатой
	uint32_t selectDelayOff;					///< Время удержания кнопки выбора отпущенной

	uint64_t burstTime;				///< Время начала отсчета интервала меджу снимками
	uint32_t burstDelay;				///< Временная задержка между снимками
	bool burstFlag;					///< Флаг редима серийной съемки

	bool recordState;					///< Состояние режима записи видео
	uint64_t recordTime;				///< Время нажатия кнопки включения/выключения записи
	bool recordFlag;					///< Флаг нажатия кнопки включения/выключения записи

	GoProGlobalSettings_TypeDef settingsGlobalCurrent;		///< Текущий режим настройки камеры
	bool settingsGlobalFlag;			///< Флаг включенного меню настроек

	bool settingsState;					///< Текушее состояние режима настройки
	uint64_t settingsTime;				///< Время нажатия кнопки включение/выключение режима настройки
	bool settingsFlag;					///< Флаг нажатия кнопки включение/выключение режима настройки

	GpioName::Gpio* pinMode;		///< Вывод управления кнопкой изменения режима и включения/выключения питания камеры
	GpioName::Gpio* pinSelect;		///< Вывод управления кнопкой выбора и включения/выключения записи
	GpioName::Gpio* pinSettings;	///< Вывод управления кнопкой включения/выключения режима настройки


	uint8_t iterModeButtonPress1;		///< Счетчик выполненых нажатий на кнопку Mode  (первый уровень меню)
	uint8_t iterSelectButtonPress1;		///< Счетчик выполненых нажатий на кнопку Select (выбор пункта меню первого уровеня)
	uint8_t iterModeButtonPress2;		///< Счетчик выполненых нажатий на кнопку Mode  (второй уровень меню)
	uint8_t iterSelectButtonPress2;		///< Счетчик выполненых нажатий на кнопку Select (выбор пункта меню второго уровеня)
	uint8_t iterModeButtonPress3;		///< Счетчик выполненых нажатий на кнопку Mode  (третий уровень меню)
	uint8_t iterSelectButtonPress3;		///< Счетчик выполненых нажатий на кнопку Select (выбор пункта меню третьего уровеня)
	uint8_t iterModeButtonPress4;		///< Счетчик выполненых нажатий на кнопку Mode  (подтверждение)
	uint8_t iterSelectButtonPress4;		///< Счетчик выполненых нажатий на кнопку Select (выбор пункта подтверждения)

public:
	GoPro(GpioName::GpioPort_TypeDef portMode, uint8_t pinMode,
			GpioName::GpioPort_TypeDef portSelect, uint8_t pinSelect,
			GpioName::GpioPort_TypeDef portSettings, uint8_t pinSettings);
	void Power(bool state);
	void Record(bool state);
	void TakeFoto();
	void FormatMemory();
	void Select(bool state);
	void Settings(GoProButton_TypeDef buttonState);
	void ChangeMode(GoProMode_TypeDef mode);
	void UpdateMode(uint8_t numberModeButtonPress1, uint8_t numberSelectButtonPress1,
			uint8_t numberModeButtonPress2, uint8_t numberSelectButtonPress2,
			uint8_t numberModeButtonPress3, uint8_t numberSelectButtonPress3,
			uint8_t numberModeButtonPress4, uint8_t numberSelectButtonPress4);
	void BurstMode(bool value, uint32_t delay);
	void Update();
};


}  // namespace GoProName

#endif /* INCLUDE_GOPRO_H_ */
