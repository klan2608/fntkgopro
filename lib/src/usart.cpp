/*
 * usart.cpp
 *
 *  Created on: 19 апр. 2015 г.
 *      Author: klan
 */
#include "../lib/include/usart.h"

using namespace UsartName;

Usart::Usart(Usart_TypeDef usartName, uint32_t baudRate)
{
	state = UsartState_Init;
	this->usartName = usartName;
	dataSendingComplete = true;
	BufferForSendQueueHoldOn = false;
	DataForSendSizeQueue = 0;
	memset(BufferForSend, 0, sizeof(BufferForSend));
	memset(BufferForRead, 0, sizeof(BufferForRead));
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;

	USART_InitTypeDef USART_InitStructure;
	USART_InitStructure.USART_BaudRate = baudRate;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;

	DMA_InitTypeDef DMA_InitStructure;
//	DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
//	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
//	DMA_InitStructure.DMA_BufferSize = (uint32_t) sizeof(BufferForSend);
//	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t) &BufferForSend;
//	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) &(USART2->DR);
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;

	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;

	switch(usartName)
	{
	case Usart_1:		// TX - PA9, RX - PA10
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_10;
		NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel4_IRQn;
		port = GPIOA;
		usart = USART1;
		dmaChannelRx = DMA1_Channel5;
		dmaChannelTx = DMA1_Channel4;
		break;
	case Usart_1_Remap:		// TX - PB6, RX - PB7
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
		NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel4_IRQn;
		port = GPIOB;
		usart = USART1;
		dmaChannelRx = DMA1_Channel5;
		dmaChannelTx = DMA1_Channel4;
		break;
	case Usart_2:		// TX - PA2, RX - PA3
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_3;
		NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel7_IRQn;
		port = GPIOA;
		usart = USART2;
		dmaChannelRx = DMA1_Channel6;
		dmaChannelTx = DMA1_Channel7;
		break;
	default:
		state = UsartState_Error;
	}

	if(state == UsartState_Init)
	{
		GPIO_Init(port, &GPIO_InitStructure);
		USART_Init(usart, &USART_InitStructure);
		//DMA TX Init
		DMA_DeInit(dmaChannelTx);
		DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
		DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
		DMA_InitStructure.DMA_BufferSize = (uint32_t) sizeof(BufferForSend);
		DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t) &BufferForSend;
		DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) &(usart->DR);
		DMA_Init(dmaChannelTx, &DMA_InitStructure);
		DMA_Cmd(dmaChannelTx, DISABLE);
		USART_DMACmd(usart, USART_DMAReq_Tx, ENABLE);
		DMA_ITConfig(dmaChannelTx, DMA_IT_TC, ENABLE);
		NVIC_Init(&NVIC_InitStructure);
		//DMA RX Init
		DMA_DeInit(dmaChannelRx);
		DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
		DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
		DMA_InitStructure.DMA_BufferSize = 127;
		//DMA_InitStructure.DMA_BufferSize = (uint32_t) sizeof(BufferForRead);
		DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t) &BufferForRead;
		DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) &(usart->DR);
		DMA_Init(dmaChannelRx, &DMA_InitStructure);
		DMA_Cmd(dmaChannelRx, ENABLE);
		USART_DMACmd(usart, USART_DMAReq_Rx, ENABLE);

		USART_Cmd(usart, ENABLE);
		state = UsartState_On;
	}


}

Usart::~Usart()
{

}

void Usart::Switch(UsartState_TypeDef state)
{
	if(state == UsartState_On)
	{
		DMA_Cmd(dmaChannelTx, ENABLE);
		DMA_Cmd(dmaChannelRx, ENABLE);
		USART_Cmd(usart, ENABLE);
	}
	else if(state == UsartState_Off)
	{
		DMA_Cmd(dmaChannelTx, DISABLE);
		DMA_Cmd(dmaChannelRx, DISABLE);
		USART_Cmd(usart, DISABLE);
	}
}

void Usart::SendData(uint8_t* data, uint16_t size)
{
	if((DataForSendSizeQueue + size) <= sizeof(BufferForSendQueue))
	{
		BufferForSendQueueHoldOn = true;
		memcpy(BufferForSendQueue+DataForSendSizeQueue, data, size);
		DataForSendSizeQueue += size;
		BufferForSendQueueHoldOn = false;
	}
	StartTransmite();
}

void Usart::StartTransmite()
{
	if(dataSendingComplete && DataForSendSizeQueue <= sizeof(BufferForSend) && DataForSendSizeQueue > 0 && !BufferForSendQueueHoldOn)
	{
		memcpy(BufferForSend, BufferForSendQueue, DataForSendSizeQueue);
		dataSendingComplete = false;
		DMA_SetCurrDataCounter(dmaChannelTx, DataForSendSizeQueue);
		DataForSendSizeQueue = 0;
		DMA_Cmd(dmaChannelTx, ENABLE);
	}
}

//void Usart::SendData(uint8_t* data, uint16_t size)
//{
//	if((size <= sizeof(BufferForSend)) && (dataSendingComplete == true))
//	{
//		memset(BufferForSend, 0, sizeof(BufferForSend));
//		memcpy(BufferForSend, data, size);
//		dataSendingComplete = false;
//		DMA_SetCurrDataCounter(dmaStreamTx, size);
//		DMA_Cmd(dmaStreamTx, ENABLE);
//	}
//}

uint8_t* Usart::GetReadData(void)
{
	return BufferForRead;
}

uint16_t Usart::GetReadDataSize(void)
{
	uint16_t sizeDMA = DMA_GetCurrDataCounter(dmaChannelRx);
	uint16_t sizeBuffer = sizeof(BufferForRead);

	return (uint16_t)(sizeBuffer - sizeDMA - 1);

}

uint8_t* Usart::GetSendData(void)
{
	return BufferForRead;
}

uint16_t Usart::GetSendDataSize(void)
{
	uint16_t sizeDMA = DMA_GetCurrDataCounter(dmaChannelRx);
	uint16_t sizeBuffer = sizeof(BufferForRead);

	return (uint16_t)(sizeBuffer - sizeDMA - 1);

}

