/*
 * gpio.cpp
 *
 *  Created on: 14 апр. 2015 г.
 *      Author: klan
 */

#include "../lib/include/gpio.h"

using namespace GpioName;


Gpio::Gpio(GpioPort_TypeDef portInput, uint8_t pinInput, GpioMode_TypeDef modeInput)
{
	Init(portInput, pinInput, modeInput);
}

Gpio::~Gpio()
{
	Deinit();
}

void Gpio::Init(GpioPort_TypeDef port, uint8_t pin, GpioMode_TypeDef mode)
{
	state = GpioState_Init;
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;

	EXTI_InitTypeDef EXTI_InitStructure;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;

	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);

	if(mode == GpioMode_Normal_OD || mode == GpioMode_Inverted_OD)
 	{
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;
	}
	else if(mode == GpioMode_Int_Falling || mode == GpioMode_Int_Falling_OD)
	{
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
		EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
	}
	else if (mode == GpioMode_Int_Rising || mode == GpioMode_Int_Rising_OD)
	{
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
		EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
	}

	if(mode == GpioMode_Int_Falling_OD || mode == GpioMode_Int_Rising_OD)
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;

	if(mode == GpioMode_Normal || mode == GpioMode_Normal_OD)
	{
		offLevel = GpioLevel_Low;
	}
	else if(mode == GpioMode_Inverted || mode == GpioMode_Inverted_OD)
	{
		offLevel = GpioLevel_High;
	}

	if(pin < 16)
	{
		this->pin = 1 << pin;
		this->exti = EXTI_Line0 << pin;
	}
	else
		state = GpioState_Error;

	switch(port)
	{
	case GpioPort_A:
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
		this->port = GPIOA;
		break;
	case GpioPort_B:
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
		this->port = GPIOB;
		break;
	case GpioPort_C:
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
		this->port = GPIOC;
		break;
	case GpioPort_D:
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD, ENABLE);
		this->port = GPIOD;
		break;
	case GpioPort_E:
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOE, ENABLE);
		this->port = GPIOE;
		break;
	case GpioPort_F:
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOF, ENABLE);
		this->port = GPIOF;
		break;
	case GpioPort_G:
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOG, ENABLE);
		this->port = GPIOG;
		break;
	default:
		state = GpioState_Error;
		break;
	}

	if(state == GpioState_Init)
	{
		GPIO_InitStructure.GPIO_Pin = this->pin;
		GPIO_Init(this->port, &GPIO_InitStructure);
		if(mode == GpioMode_Int_Falling || mode == GpioMode_Int_Falling_OD ||
				mode == GpioMode_Int_Rising || mode == GpioMode_Int_Rising_OD)
		{
			EXTI_InitStructure.EXTI_Line = exti;
			EXTI_Init(&EXTI_InitStructure);

			if(pin <= 4)
			{
				NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn+pin;
			}
			else if(pin > 4 && pin <=9)
			{
				NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;
			}
			else if(pin > 9 && pin <= 15)
			{
				NVIC_InitStructure.NVIC_IRQChannel = EXTI15_10_IRQn;
			}
			NVIC_Init(&NVIC_InitStructure);
		}
		else
			GPIO_WriteBit(this->port, this->pin, (BitAction) this->offLevel);
		state = GpioState_Off;
	}
}

GpioState_TypeDef Gpio::GetState()
{
	return state;
}

void Gpio::Deinit()
{
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_StructInit(&GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Pin = this->pin;
	GPIO_Init(this->port, &GPIO_InitStructure);
}

void Gpio::SetLevel(GpioLevel_TypeDef level)
{
	if(level == GpioLevel_High || level == GpioLevel_Low)
		GPIO_WriteBit(port, pin, (BitAction) level);
}

void Gpio::Switch(GpioState_TypeDef state)
{
	GPIO_WriteBit(port, pin, (BitAction) (offLevel ^ state));
}

void Gpio::Toggle()
{
	state = (GpioState_TypeDef) (GPIO_ReadOutputDataBit(port, pin) ^ offLevel);
	Switch((GpioState_TypeDef) !state);
}
