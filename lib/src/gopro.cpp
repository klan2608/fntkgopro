/**
 * @file gopro.cpp
 * @author Y.Reznikov
 * @version 1.0.0
 * @date Июль 2015 года
*/

#include <gopro.h>
#include <stdlib.h>

using namespace GoProName;
using namespace GpioName;

/**
  * @brief  Настройка выводов для управления кнопками камеры
  *
  * Используется пралельное подключение к кнопке
  * @param  portMode порт вывода для управления кнопкой изменения режима и включения/выключения камеры. Значение должно быть согластно списка GpioName::GpioPort_TypeDef
  * @param pinMode номер вывода для управления кнопкой изменения режима и включения/выключения камеры. Значение должно быть от 0 до 15
  * @param  portSelect порт вывода для управления кнопкой выбора и включения/выключения записи. Значение должно быть согластно списка GpioName::GpioPort_TypeDef
  * @param pinSelect номер вывода для управления кнопкой выбора и включения/выключения записи. Значение должно быть от 0 до 15
  * @param  portSettings порт вывода для управления кнопкой включения/выключения режима настройки. Значение должно быть согластно списка GpioName::GpioPort_TypeDef
  * @param pinSettings номер вывода для управления кнопкой включения/выключения режима настройки. Значение должно быть от 0 до 15
  * @retval Нет
  */
GoPro::GoPro(GpioName::GpioPort_TypeDef portMode, uint8_t pinMode,
		GpioName::GpioPort_TypeDef portSelect, uint8_t pinSelect,
		GpioName::GpioPort_TypeDef portSettings, uint8_t pinSettings)
{
	this->pinSettings = new Gpio(portSettings, pinSettings, GpioMode_Inverted_OD);
	this->pinSettings->Switch(GpioState_Off);
	this->pinSelect = new Gpio(portSelect, pinSelect, GpioMode_Inverted_OD);
	this->pinSelect->Switch(GpioState_Off);
	this->pinMode = new Gpio(portMode, pinMode, GpioMode_Inverted_OD);
	this->pinMode->Switch(GpioState_Off);

	powerState = false;
	powerTime = 0;
	powerFlag = false;

	modeTime = 0;
	modeFlag = false;
	modeFlagDelay = false;
	modeDelayOn = 100000;
	modeDelayOff = 100000;
	modeCurrent = GoProMode_Video;

	recordState = false;
	recordFlag = false;
	recordTime = 0;

	selectFlag = false;
	selectFlagDelay = false;
	selectDelayOn = 100000;
	selectDelayOff = 1800000;
	selectTime = 0;

	burstFlag = false;
	burstTime = 0;
	burstDelay = 0;

	settingsGlobalFlag = false;
	settingsGlobalCurrent = GoProGlobalSettings_WiFi;

	settingsState = false;
	settingsTime = 0;
	settingsFlag = false;

	iterModeButtonPress1 = 0;
	iterSelectButtonPress1 = 0;
	iterModeButtonPress2 = 0;
	iterSelectButtonPress2 = 0;
	iterModeButtonPress3 = 0;
	iterSelectButtonPress3 = 0;
	iterModeButtonPress4 = 0;
	iterSelectButtonPress4 = 0;

}

/**
  * @brief  Включение/выключение питания камеры
  *
  * Используется пралельное подключение к кнопке Mode и удержании ее в течении 2 секунд
  * @param  state состояние питания камеры. true - включение, false - выключение
  * @retval Нет
  */
void GoPro::Power(bool state)
{
	if(powerState != state && !powerFlag)
	{
		this->pinMode->Switch(GpioState_On);
		powerTime = DelayName::Delay::GetMicrosecond();
		powerFlag = true;
		powerState = state;
	}
}

/**
  * @brief  Включение/выключение записи видео, одиночный снимок, многокадровый снимок
  *
  * Используется пралельное подключение к кнопке Select и удержании ее в течении 0.5 секунд.
  * В режиме видеосъемки - запись видео. Режим не срабатывает, если уже включен режим серийной съемки, в остальных случаях происходит автоматическое переключение режима на Видео
  * @param  state состояние записи. true - включение, false - выключение
  * @retval Нет
  */
void GoPro::Record(bool state)
{
	if(recordState != state && powerState && !powerFlag && !burstFlag && !selectFlag && !modeFlag)
	{
		uint8_t deltaMode = 0;
		uint8_t deltaSettings = 0;
		if(settingsGlobalFlag)
		{
			deltaSettings = ((uint8_t) GoProGlobalSettings_Exit + GoProGlobalSettings_Exit + 1 - (uint8_t)settingsGlobalCurrent) % (GoProGlobalSettings_Exit + 1);
			deltaMode = ((uint8_t) GoProMode_Video + GoProMode_GlobalSettings + 1 - (uint8_t)GoProMode_Video) % (GoProMode_GlobalSettings + 1);
			UpdateMode(deltaSettings, 1, deltaMode, 1, 0, 0, 0, 0);
			settingsGlobalFlag = false;
		}
		else
		{
			deltaMode = ((uint8_t) GoProMode_Video + GoProMode_GlobalSettings + 1 - (uint8_t)modeCurrent) % (GoProMode_GlobalSettings + 1);
			UpdateMode(deltaMode, 1, 0, 0, 0, 0, 0, 0);
		}
		modeCurrent = GoProMode_Video;
		recordState = state;
	}
}

/**
  * @brief  Сделать снимок
  *
  * Используется пралельное подключение к кнопке Select и удержании ее в течении 0.5 секунд. Режим не срабатывает, если уже включен режим записи видео, в остальных случаях происходит автоматическое переключение режима на Фото
  * @param  Нет
  * @retval Нет
  */
void GoPro::TakeFoto()
{
	if(powerState && !powerFlag && !recordState && !selectFlag && !modeFlag)
	{
		uint8_t deltaMode = 0;
		uint8_t deltaSettings = 0;
		if(settingsGlobalFlag)
		{
			deltaSettings = ((uint8_t) GoProGlobalSettings_Exit + GoProGlobalSettings_Exit + 1 - (uint8_t)settingsGlobalCurrent) % (GoProGlobalSettings_Exit + 1);
			deltaMode = ((uint8_t) GoProMode_Photo + GoProMode_GlobalSettings + 1 - (uint8_t)GoProMode_Video) % (GoProMode_GlobalSettings + 1);
			UpdateMode(deltaSettings, 1, deltaMode, 1, 0, 0, 0, 0);
			settingsGlobalFlag = false;
		}
		else
		{
			deltaMode = ((uint8_t) GoProMode_Photo + GoProMode_GlobalSettings + 1 - (uint8_t)modeCurrent) % (GoProMode_GlobalSettings + 1);
			UpdateMode(deltaMode, 1, 0, 0, 0, 0, 0, 0);
		}
		modeCurrent = GoProMode_Photo;
	}
}

/**
  * @brief  Форматирование карты памяти
  *
  * Выполняется удаление всех файлов на карте памяти. Режим не срабатывает, если уже включен режим записи видео или серийной съемки.
  * @param  Нет
  * @retval Нет
  */
void GoPro::FormatMemory()
{
	if(powerState && !powerFlag && !burstFlag && !recordState && !selectFlag && !modeFlag)
	{
		uint8_t deltaMode = ((uint8_t) GoProMode_GlobalSettings + GoProMode_GlobalSettings + 1 - (uint8_t)modeCurrent) % (GoProMode_GlobalSettings + 1);
		UpdateMode(deltaMode, 1, GoProGlobalSettings_Delete, 1, 1, 1, 1, 1);
		modeCurrent = GoProMode_GlobalSettings;
		settingsGlobalCurrent = GoProGlobalSettings_Delete;
		settingsGlobalFlag = true;
	}
}

/**
  * @brief  Переключение режима
  *
  * Используется пралельное подключение к кнопке Mode и удержании ее в течении 0.5 секунды.
  * @param  mode режим работы камеры. Значение должно быть согластно списка GoProMode_TypeDef
  * @retval Нет
  */
void GoPro::ChangeMode(GoProMode_TypeDef mode)
{
	if(powerState && !powerFlag)
	{
		uint8_t deltaMode = 0;
		uint8_t deltaSettings = 0;
		if(settingsGlobalFlag)
		{
			deltaSettings = ((uint8_t) GoProGlobalSettings_Exit + GoProGlobalSettings_Exit + 1 - (uint8_t)settingsGlobalCurrent) % (GoProGlobalSettings_Exit + 1);
			deltaMode = ((uint8_t) mode + GoProMode_GlobalSettings + 1 - (uint8_t)GoProMode_Video) % (GoProMode_GlobalSettings + 1);
			UpdateMode(deltaSettings, 1, deltaMode, 0, 0, 0, 0, 0);
			settingsGlobalFlag = false;
		}
		else
		{
			deltaMode = ((uint8_t) mode + GoProMode_GlobalSettings + 1 - (uint8_t)modeCurrent) % (GoProMode_GlobalSettings + 1);
			UpdateMode(deltaMode, 0, 0, 0, 0, 0, 0, 0);
		}
		modeCurrent = mode;
	}
}

/**
  * @brief Серийная съемка
  *
  * Камера автоматически выполняет снимки между заданными интервалами времени
  * @param  delay время между снимками. Значение устанавлевается в микросекундах
  * @retval Нет
  */
void GoPro::BurstMode(bool value, uint32_t delay)
{
	if(powerState && !powerFlag && !recordState)
	{
		burstDelay = delay;
		burstFlag = value;
		if(value)
		{
			burstTime = DelayName::Delay::GetMicrosecond();
			TakeFoto();
		}
	}
}

/**
  * @brief  Настройка нажатий на кнопки Mode, Select
  *
  * Устанавливается количество и посследовательность на кнопки для переключения режимов
  * @param numberModeButtonPress1 колличество нажатий на кпоку Mode (первый уровень меню)
  * @param numberSelectButtonPress1 колличество нажатий на кпоку Select (выбор пункта меню первого уровня)
  * @param numberModeButtonPress2 колличество нажатий на кпоку Mode (второй уровень меню)
  * @param numberSelectButtonPress2 колличество нажатий на кпоку Select (выбор пункта меню второго уровня)
  * @param numberModeButtonPress3 колличество нажатий на кпоку Mode (третий уровень меню)
  * @param numberSelectButtonPress3 колличество нажатий на кпоку Select (выбор пункта меню третьего уровня)
  * @param numberModeButtonPress4 колличество нажатий на кпоку Mode (подтверждение)
  * @param numberSelectButtonPress4 колличество нажатий на кпоку Select (выбор пункта подтверждения)
  * @retval Нет
  */
void GoPro::UpdateMode(uint8_t numberModeButtonPress1, uint8_t numberSelectButtonPress1,
		uint8_t numberModeButtonPress2, uint8_t numberSelectButtonPress2,
		uint8_t numberModeButtonPress3, uint8_t numberSelectButtonPress3,
		uint8_t numberModeButtonPress4, uint8_t numberSelectButtonPress4)
{
	iterModeButtonPress1 = numberModeButtonPress1;
	iterSelectButtonPress1 = numberSelectButtonPress1;
	iterModeButtonPress2 = numberModeButtonPress2;
	iterSelectButtonPress2 = numberSelectButtonPress2;
	iterModeButtonPress3 = numberModeButtonPress3;
	iterSelectButtonPress3 = numberSelectButtonPress3;
	iterModeButtonPress4 = numberModeButtonPress4;
	iterSelectButtonPress4 = numberSelectButtonPress4;
	if(iterModeButtonPress1 > 0)
	{
		pinMode->Switch(GpioState_On);
		modeTime = DelayName::Delay::GetMicrosecond();
		modeFlag = true;
		iterModeButtonPress1--;
		return;
	}
	if(iterSelectButtonPress1 > 0)
	{
		pinSelect->Switch(GpioState_On);
		selectTime = DelayName::Delay::GetMicrosecond();
		selectFlag = true;
		iterSelectButtonPress1--;
		return;
	}
	if(iterModeButtonPress2 > 0)
	{
		pinMode->Switch(GpioState_On);
		modeTime = DelayName::Delay::GetMicrosecond();
		modeFlag = true;
		iterModeButtonPress2--;
		return;
	}
	if(iterSelectButtonPress2 > 0)
	{
		pinSelect->Switch(GpioState_On);
		selectTime = DelayName::Delay::GetMicrosecond();
		selectFlag = true;
		iterSelectButtonPress2--;
		return;
	}
	if(iterModeButtonPress3 > 0)
	{
		pinMode->Switch(GpioState_On);
		modeTime = DelayName::Delay::GetMicrosecond();
		modeFlag = true;
		iterModeButtonPress3--;
		return;
	}
	if(iterSelectButtonPress3 > 0)
	{
		pinSelect->Switch(GpioState_On);
		selectTime = DelayName::Delay::GetMicrosecond();
		selectFlag = true;
		iterSelectButtonPress3--;
		return;
	}
	if(iterModeButtonPress4 > 0)
	{
		pinMode->Switch(GpioState_On);
		modeTime = DelayName::Delay::GetMicrosecond();
		modeFlag = true;
		iterModeButtonPress4--;
		return;
	}
	if(iterSelectButtonPress4 > 0)
	{
		pinSelect->Switch(GpioState_On);
		selectTime = DelayName::Delay::GetMicrosecond();
		selectFlag = true;
		iterSelectButtonPress4--;
		return;
	}
}

/**
  * @brief  Обновление задержек
  *
  * Определение моментов времени для размыкания кнопок управления, выполнения задержек между повторными нажатиями
  * @param Нет
  * @retval Нет
  */
void GoPro::Update()
{
	if(powerFlag)
	{
		if(DelayName::Delay::GetMicrosecond() - powerTime > 3000000)
		{
			powerFlag = false;
			pinMode->Switch(GpioState_Off);
		}
	}
	if(modeFlag)
	{
		if(DelayName::Delay::GetMicrosecond() - modeTime > modeDelayOn)
		{
			modeFlagDelay = true;
			modeFlag = false;
			pinMode->Switch(GpioState_Off);
			modeTime = DelayName::Delay::GetMicrosecond();
		}
	}
	if(modeFlagDelay)
	{
		if(DelayName::Delay::GetMicrosecond() - modeTime > modeDelayOff)
		{
			modeFlagDelay = false;
			modeTime = DelayName::Delay::GetMicrosecond();
			UpdateMode(iterModeButtonPress1, iterSelectButtonPress1,
					iterModeButtonPress2, iterSelectButtonPress2,
					iterModeButtonPress3, iterSelectButtonPress3,
					iterModeButtonPress4, iterSelectButtonPress4);
		}
	}
	if(selectFlag)
	{
		if(DelayName::Delay::GetMicrosecond() - selectTime > selectDelayOn)
		{
			selectFlagDelay = true;
			selectFlag = false;
			pinSelect->Switch(GpioState_Off);
			selectTime = DelayName::Delay::GetMicrosecond();
		}
	}
	if(selectFlagDelay)
	{
		if(DelayName::Delay::GetMicrosecond() - selectTime > selectDelayOff)
		{
			selectFlagDelay = false;
			selectTime = DelayName::Delay::GetMicrosecond();
			UpdateMode(iterModeButtonPress1, iterSelectButtonPress1,
					iterModeButtonPress2, iterSelectButtonPress2,
					iterModeButtonPress3, iterSelectButtonPress3,
					iterModeButtonPress4, iterSelectButtonPress4);
		}
	}
	if(burstFlag)
	{
		if(DelayName::Delay::GetMicrosecond() - burstTime > burstDelay)
		{
			TakeFoto();
			burstTime = DelayName::Delay::GetMicrosecond();
		}
	}
}

