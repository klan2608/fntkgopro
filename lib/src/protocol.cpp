/*
 * protocol.cpp
 *
 *  Created on: 20 апр. 2015 г.
 *      Author: klan
 */
#include "../lib/include/protocol.h"

using namespace ProtocolName;
using namespace ConfigName;


Protocol::Protocol(uint16_t sizeBuffer)
{
	config.inputAngles.roll = 0.f;
	config.inputAngles.pitch = -90.f;
	config.inputAngles.yaw = 0.f;
	config.inputAngularVelocity.roll = 0.f;
	config.inputAngularVelocity.pitch = 0.f;
	config.inputAngularVelocity.yaw = 0.f;
	config.outputAngles.roll = 0.f;
	config.outputAngles.pitch = -90.f;
	config.outputAngles.yaw = 0.f;
	config.requestState = false;
	config.newData.requestStateChange = false;
	config.settings.cameraMode = SettingCameraMode_TV;
	config.newData.cameraModeChange = false;
	config.settings.colorMode = 1;
	config.newData.colorModeChange = false;
	config.settings.controlPWM = false;
	config.newData.controlPWMChange = false;
	config.settings.exposure = 0;
	config.newData.exposureChange = false;
	config.settings.frequencyPosition = 0;
	config.newData.frequencyPositionChange = false;
	config.settings.photoMode.mode = PhotoMode_NoShot;
	config.newData.photoModeChange = false;
	config.settings.power = SettingPower_On;
	config.newData.powerChange = false;
	config.settings.stabilization = false;
	config.newData.stabilizationChange = false;
	config.settings.videoMode = SettingVideoMode_VBS;
	config.newData.videoModeChange = false;
	config.settings.focus = SettingFocus_Off;
	config.newData.focusChange = false;
	config.settings.calibration = false;
	config.newData.calibrationChange = false;
	config.settings.zoom = 1;
	config.newData.zoomChange = false;

	config.configPID.roll.request = false;
	config.configPID.roll.Kp = 0.0f;
	config.configPID.roll.scopeKp = 0.0f;
	config.configPID.roll.Ki = 0.0f;
	config.configPID.roll.scopeKi = 0.0f;
	config.configPID.roll.Kd = 0.0f;
	config.configPID.roll.scopeKd = 0.0f;
	config.configPID.roll.L = 100.0f;
	config.configPID.pitch.request = false;
	config.configPID.pitch.Kp = 0.0f;
	config.configPID.pitch.scopeKp = 0.0f;
	config.configPID.pitch.Ki = 0.0f;
	config.configPID.pitch.scopeKi = 0.0f;
	config.configPID.pitch.Kd = 0.0f;
	config.configPID.pitch.scopeKd = 0.0f;
	config.configPID.pitch.L = 100.0f;
	config.configPID.yaw.request = false;
	config.configPID.yaw.Kp = 0.0f;
	config.configPID.yaw.scopeKp = 0.0f;
	config.configPID.yaw.Ki = 0.0f;
	config.configPID.yaw.scopeKi = 0.0f;
	config.configPID.yaw.Kd = 0.0f;
	config.configPID.yaw.scopeKd = 0.0f;
	config.configPID.yaw.L = 100.0f;
	config.newData.pidChange = false;

	config.newData.requestCalibData = false;

	config.newData.sensorZero = false;

	this->sizeInputBuffer = sizeBuffer;

	head = tail = 0;

	sizeOutputBuffer = sizeof(buffer);
	memset(buffer, 0, this->sizeOutputBuffer);
	eepromID = 0;
}

void Protocol::SetConfig(ConfigurationMUSV_TypeDef* config)
{
	this->config.outputAngles.roll = config->outputAngles.roll;
	this->config.outputAngles.pitch = config->outputAngles.pitch;
	this->config.outputAngles.yaw = config->outputAngles.yaw;

	this->config.requestState = config->requestState;
	this->config.newData.requestStateChange = config->newData.requestStateChange;

	this->config.settings.cameraMode = config->settings.cameraMode;
	this->config.newData.cameraModeChange = config->newData.cameraModeChange;
	this->config.settings.colorMode = config->settings.colorMode;
	this->config.newData.colorModeChange = config->newData.colorModeChange;
	this->config.settings.controlPWM = config->settings.controlPWM;
	this->config.newData.controlPWMChange = config->newData.controlPWMChange;
	this->config.settings.exposure = config->settings.exposure;
	this->config.newData.exposureChange = config->newData.exposureChange;
	this->config.settings.frequencyPosition = config->settings.frequencyPosition;
	this->config.newData.frequencyPositionChange = config->newData.frequencyPositionChange;
	this->config.settings.photoMode = config->settings.photoMode;
	this->config.newData.photoModeChange = config->newData.photoModeChange;
	this->config.settings.power = config->settings.power;
	this->config.newData.powerChange = config->newData.powerChange;
	this->config.settings.stabilization = config->settings.stabilization;
	this->config.newData.stabilizationChange = config->newData.stabilizationChange;
	this->config.settings.videoMode = config->settings.videoMode;
	this->config.newData.videoModeChange = config->newData.videoModeChange;
	this->config.settings.zoom = config->settings.zoom;
	this->config.newData.zoomChange = config->newData.zoomChange;

	this->config.configPID.roll.request = config->configPID.roll.request;
	this->config.configPID.roll.Kp = config->configPID.roll.Kp;
	this->config.configPID.roll.scopeKp = config->configPID.roll.scopeKp;
	this->config.configPID.roll.Ki = config->configPID.roll.Ki;
	this->config.configPID.roll.scopeKi = config->configPID.roll.scopeKi;
	this->config.configPID.roll.Kd = config->configPID.roll.Kd;
	this->config.configPID.roll.scopeKd = config->configPID.roll.scopeKd;
	this->config.configPID.roll.L = config->configPID.roll.L;
	this->config.configPID.pitch.request = config->configPID.pitch.request;
	this->config.configPID.pitch.Kp = config->configPID.pitch.Kp;
	this->config.configPID.pitch.scopeKp = config->configPID.pitch.scopeKp;
	this->config.configPID.pitch.Ki = config->configPID.pitch.Ki;
	this->config.configPID.pitch.scopeKi = config->configPID.pitch.scopeKi;
	this->config.configPID.pitch.Kd = config->configPID.pitch.Kd;
	this->config.configPID.pitch.scopeKd = config->configPID.pitch.scopeKd;
	this->config.configPID.pitch.L = config->configPID.pitch.L;
	this->config.configPID.yaw.request = config->configPID.yaw.request;
	this->config.configPID.yaw.Kp = config->configPID.yaw.Kp;
	this->config.configPID.yaw.scopeKp = config->configPID.yaw.scopeKp;
	this->config.configPID.yaw.Ki = config->configPID.yaw.Ki;
	this->config.configPID.yaw.scopeKi = config->configPID.yaw.scopeKi;
	this->config.configPID.yaw.Kd = config->configPID.yaw.Kd;
	this->config.configPID.yaw.scopeKd = config->configPID.yaw.scopeKd;
	this->config.configPID.yaw.L = config->configPID.yaw.L;
	this->config.newData.pidChange = config->newData.pidChange;

	this->config.newData.requestCalibData = config->newData.requestCalibData;

	this->config.newData.sensorZero = config->newData.sensorZero;

}

ConfigurationMUSV_TypeDef* Protocol::GetConfig()
{
	return &this->config;
}

uint16_t Protocol::CheckIter(uint16_t iter)
{
	return iter % (128-1);			//DMA не записывает в последнюю ячейку данные
}


float Protocol::ByteToFloat(uint8_t* data, uint16_t startIndex)
{
	float temp;
	uint8_t arr[4];
	arr[0] = data[CheckIter(startIndex + 3)];
	arr[1] = data[CheckIter(startIndex + 2)];
	arr[2] = data[CheckIter(startIndex + 1)];
	arr[3] = data[CheckIter(startIndex)];
	memcpy(&temp, arr, sizeof(temp));
	return temp;
}

void Protocol::FloatToByte(float number, uint8_t* data, uint16_t startIndex)
{
	uint8_t* p;
	p =  (uint8_t*) &number;
	memcpy(data+startIndex, p, 4);
	uint8_t tmp;
	tmp = data[startIndex];
	data[startIndex] = data[startIndex + 3];
	data[startIndex + 3] = tmp;
	tmp = data[startIndex + 1];
	data[startIndex + 1] = data[startIndex + 2];
	data[startIndex + 2] = tmp;
}

int Protocol::FindNextHead(uint8_t* data, int startIndex, uint16_t head, uint16_t tail)
{
	int i = CheckIter(startIndex);
	if(i < head)
		return -1;
	while(i != tail)
	{
		switch(data[i])
		{
		case MSG_TYPE_UAV:
		case MSG_TYPE_STATE_REQ:
		case MSG_TYPE_ANGLES:
		case MSG_TYPE_SETTINGS:
		case MSG_TYPE_ANGULAR_SPEED:
		case MSG_TYPE_PHOTO_VIDEO:
		case MSG_TYPE_SENSOR_ZERO:
			return i;
			break;
		default:
			i = CheckIter(i+1);
			break;
		}
	}
	return -1;
}

bool Protocol::CheckSum(uint8_t* data, uint16_t index, uint16_t size)
{
	uint8_t checkSum = 0;
	uint16_t i;
	for(i = index; i != CheckIter(CheckIter(index)+data[CheckIter(index+1)]-1); i = CheckIter(i+1))
	{
		if(i == size)
			break;
		checkSum += data[i];
	}
	if(checkSum == data[i])
	{
		return true;
	}
	return false;
}

void Protocol::DecodingData(uint8_t* data, uint16_t size)
{
	tail = size;
	int newIndex = head-1;
	int lastIndex;
	do
	{
		lastIndex = newIndex;
		newIndex = FindNextHead(data, lastIndex+1, head, tail);
		if(newIndex >= 0)
		{
			if(CheckSum(data, newIndex, tail) == true)
			{
				switch(data[newIndex])
				{
				case MSG_TYPE_UAV:
					break;
				case MSG_TYPE_STATE_REQ:
					config.requestState = true;
					config.newData.requestStateChange = true;
					break;
				case MSG_TYPE_ANGLES:
					config.inputAngles.roll = ByteToFloat(data, CheckIter(newIndex+2));
					config.inputAngles.pitch = ByteToFloat(data, CheckIter(newIndex+6));
					config.inputAngles.yaw = ByteToFloat(data, CheckIter(newIndex+10));
					break;
				case MSG_TYPE_SETTINGS:
					switch(data[CheckIter(newIndex+2)])
					{
					case MSG_DATA_POWER:
						config.settings.power = (SettingPower_TypeDef) data[CheckIter(newIndex+3)];
						config.newData.powerChange = true;
						break;
					case MSG_DATA_PHOTO:
						config.settings.photoMode.mode = (PhotoMode_TypeDef) data[CheckIter(newIndex+3)];
						config.settings.photoMode.delay = (uint16_t) ((data[CheckIter(newIndex+4)] << 8) | data[CheckIter(newIndex+5)]);
						config.newData.photoModeChange = true;
						break;
					case MSG_DATA_ZOOM:
						config.settings.zoom = data[CheckIter(newIndex+3)];
						config.newData.zoomChange = true;
						break;
					case MSG_DATA_EXPOSURE:
						config.settings.exposure = (uint16_t) ((data[CheckIter(newIndex+3)] << 8) | data[CheckIter(newIndex+4)]);
						config.newData.exposureChange = true;
						break;
					case MSG_DATA_FREQUENCY:
						config.settings.frequencyPosition = data[CheckIter(newIndex+3)];
						config.newData.frequencyPositionChange = true;
						break;
					case MSG_DATA_STABILISATION:
						config.settings.stabilization = data[CheckIter(newIndex+3)];
						config.newData.stabilizationChange = true;
						break;
					case MSG_DATA_CONTROL_PWM:
						config.settings.controlPWM = data[CheckIter(newIndex+3)];
						config.newData.controlPWMChange = true;
						break;
					case MSG_DATA_COLOR:
						config.settings.colorMode = data[CheckIter(newIndex+3)];
						config.newData.colorModeChange = true;
						break;
					case MSG_DATA_CAMERA:
						config.settings.cameraMode = (SettingCameraMode_TypeDef) data[CheckIter(newIndex+3)];
						config.newData.cameraModeChange = true;
						break;
					case MSG_DATA_FOCUS:
						config.settings.focus = (SettingFocus_TypeDef) data[CheckIter(newIndex+3)];
						config.newData.focusChange = true;
						break;
					case MSG_DATA_CALIBRATION:
						config.settings.calibration = (bool) data[CheckIter(newIndex+3)];
						config.newData.calibrationChange = true;
						break;
					case MSG_DATA_MOTOR:
						config.settings.motorState = (bool) data[CheckIter(newIndex+3)];
						config.newData.motorStateChange = true;
						break;
					case MSG_DATA_FORMAT_FLASH:
						config.settings.formatFlash = true;
						break;
					default:
						break;
					}
					break;
				case MSG_TYPE_ANGULAR_SPEED:
					config.inputAngularVelocity.roll = ByteToFloat(data, CheckIter(newIndex+2));
					config.inputAngularVelocity.pitch = ByteToFloat(data, CheckIter(newIndex+6));
					config.inputAngularVelocity.yaw = ByteToFloat(data, CheckIter(newIndex+10));
					break;
				case MSG_TYPE_PHOTO_VIDEO:
					break;
				case MSG_TYPE_SENSOR_ZERO:
					config.newData.sensorZero = true;
					break;
				default:
					break;
				}
				head = CheckIter(newIndex + data[CheckIter(newIndex+1)]);
				data[newIndex] = 0;
			}
		}
		else if(lastIndex == head-1)
		{
			head = tail;
		}
	} while(newIndex >= 0);
}

void Protocol::CodingOutputAngleData(float value1, float value2, float value3)
{
	buffer[0] = MSG_TYPE_OUT_ANGLES;
	buffer[1] = 15;
	FloatToByte(value1, buffer, 2);
	FloatToByte(value2, buffer, 6);
	FloatToByte(value3, buffer, 10);
	uint8_t sum = 0;
	for(uint8_t i = 0; i < 14; i++)
		sum += buffer[i];
	buffer[14] = sum;
	sizeOutputBuffer = 15;
}

void Protocol::CodingOutputPID(uint8_t ID, float value)
{
	buffer[0] = MSG_TYPE_OUT_SERVICE;
	buffer[1] = 8;
	buffer[2] = ID;
	FloatToByte(value, buffer, 3);
	uint8_t sum = 0;
	for(uint8_t i = 0; i < 7; i++)
		sum += buffer[i];
	buffer[7] = sum;
	sizeOutputBuffer = 8;
}

void Protocol::CodingOutputCalibData(float value1,float value2,float value3,float value4,float value5,float value6)
{
	buffer[0] = MSG_TYPE_OUT_SERVICE;
	buffer[1] = 28;
	buffer[2] = MSG_DATA_SERVISE_CALIB_DATA;
	FloatToByte(value1, buffer, 3);
	FloatToByte(value2, buffer, 7);
	FloatToByte(value3, buffer, 11);
	FloatToByte(value4, buffer, 15);
	FloatToByte(value5, buffer, 19);
	FloatToByte(value6, buffer, 23);
	uint8_t sum = 0;
	for(uint8_t i = 0; i < 27; i++)
		sum += buffer[i];
	buffer[27] = sum;
	sizeOutputBuffer = 28;
}


void Protocol::CodingOutputStateData(ConfigurationMUSV_TypeDef* config)
{
	buffer[0] = MSG_TYPE_OUT_STATE;
	buffer[1] = 12;
	buffer[2] = config->settings.cameraMode & 0x07;					// 0-2 bit 0 byte
	buffer[2] |= (config->settings.power & 0x03) << 3;				// 3-4 bit 0 byte
	buffer[2] |= (config->settings.videoMode & 0x07) << 5;			// 5-7 bit 0 byte
	buffer[3] = config->settings.photoMode.mode & 0x03;				// 0-1 bit 1 byte
	buffer[3] |= (config->settings.stabilization & 0x01) << 2;		// 2   bit 1 byte
	buffer[3] |= (config->settings.controlPWM & 0x01) << 3;			// 3   bit 1 byte
	buffer[4] = config->settings.photoMode.delay;
	buffer[5] = config->settings.photoMode.delay >> 8;
	buffer[6] = config->settings.zoom;
	buffer[7] = config->settings.exposure;
	buffer[8] = config->settings.exposure >> 8;
	buffer[9] = config->settings.frequencyPosition;
	buffer[10] = config->settings.colorMode;
	uint8_t sum = 0;
	for(uint8_t i = 0; i < 11; i++)
		sum += buffer[i];
	buffer[11] = sum;
	sizeOutputBuffer = 12;
}
